package pl.bzwbk.demo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;

import io.javalin.Javalin;
import io.javalin.embeddedserver.jetty.websocket.WsSession;

public class Chat {

	//default PORT
    static final int PORT = 8080;
    
    static final String CHATROOM = System.getenv("HOSTNAME");

    static final int MESSAGE_TYPE_TEXT = 0;

    static Map<WsSession, String> userUsernameMap = new ConcurrentHashMap<>();
    static int nextUserNumber = 1;

    public static void main(String[] args) {
        Javalin.create()
            .port(PORT)
            .enableStaticFiles("/web")
            .ws("/chat", ws -> {
                ws.onConnect(session -> {
                    String username = "User" + nextUserNumber++;
                    userUsernameMap.put(session, username);
                    broadcastMessage(new Message("Server", (username + " joined the chat")));
                });
                ws.onClose((session, status, message) -> {
                    String username = userUsernameMap.get(session);
                    userUsernameMap.remove(session);
                    broadcastMessage(new Message("Server", (username + " left the chat")));
                });
                ws.onMessage((session, message) -> {
                	JSONObject jo = new JSONObject(message);
                	Message msg = new Message(userUsernameMap.get(session), jo.getString("text"), jo.getInt("type"));
                    broadcastMessage(msg);
                });
            })
            .start();
    }

    static void broadcastMessage(Message message) {
        userUsernameMap.keySet().stream().filter(Session::isOpen).forEach(session -> {
            try {
            	String content = new JSONObject()
                        .put("message", message.toJSONObject())
                        .put("userlist", userUsernameMap.values())
            			.put("chatroom", Chat.CHATROOM).toString();
                session.send(content);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    static class Message {
    	private String sender;
    	private String text;
    	private int type;
    	private String timestamp;

    	Message(String sender, String text) {
    		this(sender, text, MESSAGE_TYPE_TEXT);
    	}

    	Message(String sender, String text, int type) {
    		this.sender = sender;
    		this.text = text;
    		this.type = type;
    		this.timestamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
    	}

    	public JSONObject toJSONObject() {
    		return new JSONObject()
    				.put("sender", sender)
    				.put("text", text)
    				.put("type", type)
    				.put("timestamp", timestamp);
    	}
    }
}
