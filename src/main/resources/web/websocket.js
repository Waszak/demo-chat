const MESSAGE_TYPE_TEXT = 0;
const MESSAGE_TYPE_TYPING = 1;

var id = function(idValue) { return document.getElementById(idValue); };
var ws = new WebSocket(buildWsUrl());
var typingTimeout = null;

ws.onmessage = function(msg) { updateChat(msg); };
ws.onclose = function() { alert("WebSocket connection closed"); };

id("send").addEventListener("click", function() { sendMessageAndClear(buildMessage(id("message").value)); } );
id("message").addEventListener("keyup", function (e) {
    if (e.keyCode === 13) {
    	sendMessageAndClear(buildMessage(e.target.value));
    } else {
    	sendMessage(buildMessage("", MESSAGE_TYPE_TYPING));
    }
});

function buildWsUrl() {
	var wsUrl = "ws://" + location.hostname;
	if (hasText(location.port)) {
		wsUrl += ":"+ location.port;
	}
	wsUrl += "/chat";
	return wsUrl;
}

function sendMessage(message) {
    ws.send(JSON.stringify(message));
}

function sendMessageAndClear(message) {
    if (message != null) {
		sendMessage(message);
		id("message").value = "";
	}
}

function updateChat(msg) {
    let data = JSON.parse(msg.data);
    if (data.message.type == MESSAGE_TYPE_TEXT) {
    	let chatElement = id("chat");
    	chatElement.innerHTML = buildArticle(data.message) + chatElement.innerHTML;
    	id("userlist").innerHTML = data.userlist.map(function(user) { return "<li>" + user + "</li>"; }).join("");
		if (data.chatroom != null) {
			id("chatroom").innerHTML = "Chatroom <span>"+ data.chatroom +"</span>";
		}
    } else if (data.message.type == MESSAGE_TYPE_TYPING) {
    	enableTypingIndicator();
    }
}

function enableTypingIndicator() {
	if (typingTimeout != null) {
		window.clearTimeout(typingTimeout);
	}
	id("typing").style.display = "inline";
	typingTimeout = window.setTimeout(disableTypingIndicator, 512);
}

function disableTypingIndicator() {
	if (typingTimeout != null) {
		window.clearTimeout(typingTimeout);
	}
	id("typing").style.display = "none";
}

function buildMessage(msgText, msgType) {
	if (msgType == null) {
		msgType = MESSAGE_TYPE_TEXT;
	}
	if (msgType == MESSAGE_TYPE_TEXT && !hasText(msgText)) {
		return null;
	}
	return { text: msgText, type: msgType };
}

function buildArticle(message) {
	let article = "<article>";
	article += "<b>"+ message.sender +"</b>";
	article += " says: ";
	article += "<span class='timestamp'>"+ message.timestamp +"</span>";
	article += "<p>"+ message.text +"</p>";
	article += "</article>";
	return article;
}

function hasText(s) {
	return s != null && s.length > 0;
}
